#!/bin/bash

mvn -f /opt/keycloak_api/pom.xml clean package
ln -s /opt/keycloak_api/target/tuency-keycloak-api-extension.jar $KEYCLOAK_HOME/standalone/deployments
