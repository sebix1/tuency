<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020, 2021 Intevation GmbH <https://intevation.de>
 *
 * @author  2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\ClassificationType;
use App\Models\ClassificationTaxonomy;
use App\Models\FeedProvider;
use App\Models\FeedName;

class SelectorSeeder extends Seeder
{
    /* Sample values taken from IntelMQ sources */
    static $classificationTaxonomy = [
        'abusive content',
        'availability',
        'fraud',
        'intrusions',
        'malicious code',
        'vulnerable',
    ];

    /* Values taken from IntelMQ's intelmq/lib/harmonization.py */
    static $classificationType = [
        'application-compromise',
        'backdoor',
        'blacklist',
        'brute-force',
        'burglary',
        'c2server',
        'compromised',
        'copyright',
        'data-loss',
        'ddos',
        'ddos-amplifier',
        'defacement',
        'dga domain',
        'dos',
        'dropzone',
        'exploit',
        'harmful-speech',
        'ids-alert',
        'infected-system',
        'information-disclosure',
        'leak',
        'malware',
        'malware-configuration',
        'malware-distribution',
        'masquerade',
        'other',
        'outage',
        'phishing',
        'potentially-unwanted-accessible',
        'privileged-account-compromise',
        'proxy',
        'ransomware',
        'sabotage',
        'scanner',
        'sniffing',
        'social-engineering',
        'spam',
        'test',
        'tor',
        'Unauthorised-information-access',
        'Unauthorised-information-modification',
        'unauthorized-command',
        'unauthorized-login',
        'unauthorized-use-of-resources',
        'unknown',
        'unprivileged-account-compromise',
        'violence',
        'vulnerable client',
        'vulnerable service',
        'vulnerable-system',
        'weak-crypto',
    ];

    /* Sample values taken from IntelMQ sources. */
    static $feedProvider = [
        'ShadowServer',
        'Team Cymru',
    ];

    /* Sample values taken from IntelMQ sources. */
    static $feedName = [
        'Bots',
        'Brute-force Logins',
        'FTP',
        'Phishing',
    ];

    public function run()
    {
        // Disable automatic updates of the updated_by column in the seeder.
        // The seeder does not run with a specific Keycloak user, so we cannot
        // meaningfully set that column.
        config(['tuency.set_updated_by' => false]);

        // Disable auditlog. The seeder does not run with a specific Keycloak
        // user, so logging wouldn't work.
        config(['tuency.auditlog' => []]);

        foreach (static::$classificationTaxonomy as $name) {
            ClassificationTaxonomy::updateOrCreate(['name' => $name]);
        }

        foreach (static::$classificationType as $name) {
            ClassificationType::updateOrCreate(['name' => $name]);
        }

        foreach (static::$feedProvider as $name) {
            FeedProvider::updateOrCreate(['name' => $name]);
        }

        foreach (static::$feedName as $name) {
            FeedName::updateOrCreate(['name' => $name]);
        }
    }
}
