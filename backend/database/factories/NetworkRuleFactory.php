<?php

namespace Database\Factories;

use App\Models\NetworkRule;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class NetworkRuleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NetworkRule::class;


    /**
     * Define the model's default state.
     *
     * @return array
     */
    private $countRules = 0;

    public function definition()
    {
        $numNetworkRules = env('SEEDER_NUMBER_OF_NETWORK_RULES_PER_NETWORK', 1);

        $classificationTaxonomyIds = DB::table('classification_taxonomy')
            ->pluck('classification_taxonomy_id')->toArray();


        $classificationTypeIds = DB::table('classification_type')
            ->pluck('classification_type_id')->toArray();


        $feedProviderIds = DB::table('feed_provider')
            ->pluck('feed_provider_id')->toArray();

        $feedNameIds = DB::table('feed_name')
            ->pluck('feed_name_id')->toArray();

        $newUnique = false;
        if ($this->countRules == 0) {
            $newUnique = true;
        }
        $this->countRules++;

        return [
            'classification_taxonomy_id' => $this->faker->randomElement($classificationTaxonomyIds),
            'classification_type_id' => $this->faker->unique($reset = $newUnique)->randomElement($classificationTypeIds),
            'feed_provider_id' => $this->faker->randomElement($feedProviderIds),
            'feed_name_id' => $this->faker->randomElement($feedNameIds),
            'feed_status' => $this->faker->randomElement(['production', 'beta']),
            'suppress' => $this->faker->boolean(),
            'interval_length' => 1,
            'interval_unit' => 'immediate',
            'abuse_c' => false,
        ];
    }
}
