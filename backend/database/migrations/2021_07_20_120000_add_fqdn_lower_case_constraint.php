<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFqdnLowerCaseConstraint extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        UPDATE fqdn SET fqdn = lower(fqdn);
        ");
        DB::statement("
        ALTER TABLE fqdn
          ADD CONSTRAINT fqdn_fqdn_lower_check CHECK (lower(fqdn) = fqdn);
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("
        ALTER TABLE fqdn
         DROP CONSTRAINT fqdn_fqdn_lower_check;
        ");
    }
}
