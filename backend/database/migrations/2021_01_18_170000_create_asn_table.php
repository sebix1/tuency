<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asn', function (Blueprint $table) {
            $table->id('asn_id');
            $table->foreignId('organisation_id')
                  ->constrained('organisation', 'organisation_id');
            $table->bigInteger('asn');
            $table->enum('approval', ['pending', 'approved', 'denied']);
            $table->unique(['organisation_id', 'asn']);
            $table->index('asn');
            $table->index('approval');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asn');
    }
}
