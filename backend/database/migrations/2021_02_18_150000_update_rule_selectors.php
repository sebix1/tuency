<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateRuleSelectors extends Migration
{
    static $selectors = [
        'classification_taxonomy',
        'classification_type',
        'feed_provider',
        'feed_name',
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach (static::$selectors as $selector) {
            $idColumn = "${selector}_id";

            DB::table($selector)->insert([
                $idColumn => -1,
                'name' => 'any',
            ]);

            DB::table('asn_rule')
                ->whereNull($idColumn)
                ->update([$idColumn => -1]);

            Schema::table('asn_rule', function (Blueprint $table) use ($idColumn) {
                $table->unsignedBigInteger($idColumn)
                    ->nullable(false)
                    ->default(-1)
                    ->change();
            });
        }

        DB::statement("
            ALTER TABLE asn_rule
            DROP CONSTRAINT  asn_rule_feed_status_check;
        ");
        DB::statement("
            ALTER TABLE asn_rule
            ADD CONSTRAINT asn_rule_feed_status_check
                     CHECK (feed_status = ANY (ARRAY['production', 'beta', 'any']));
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        foreach (static::$selectors as $selector) {
            $idColumn = "${selector}_id";

            Schema::table('asn_rule', function (Blueprint $table) use ($idColumn) {
                $table->unsignedBigInteger($idColumn)
                    ->nullable()
                    ->change();
                $table->enum('feed_status', ['production', 'beta'])
                    ->change();
            });

            DB::table('asn_rule')
                ->where($idColumn, -1)
                ->update([$idColumn => null]);

            DB::table($selector)
                ->where($idColumn, -1)
                ->delete();
        }

        DB::statement("
            ALTER TABLE asn_rule
            DROP CONSTRAINT  asn_rule_feed_status_check;
        ");
        DB::statement("
            ALTER TABLE asn_rule
            ADD CONSTRAINT asn_rule_feed_status_check
                     CHECK (feed_status = ANY (ARRAY['production', 'beta']));
        ");
    }
}
