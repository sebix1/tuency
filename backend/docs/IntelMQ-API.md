## API for queries by IntelMQ

### Status of this document

At the time of writing this document is used with issue #127
to figure out how the API should work. Having it as a separate document
allows us to have one coherent description of the API and its data
structures and algorithms while discussing it in the issues. Once
finished it should serve as the documentation of the API.

### Overview

This document describes the tuency HTTP API which IntelMQ can use to
lookup contact information for events.

### URL

The URL for the API accepts the query parameters described in the table
below. The values of most of the parameters is the value of a field in
the IntelMQ event, and for those the description is simply the name of
the event field.

| Parameter                 | Description/event field                    |
|---------------------------|--------------------------------------------|
| `classification_taxonomy` | `classification.taxonomy`                  |
| `classification_type`     | `classification.type`                      |
| `feed_provider`           | `feed.provider`                            |
| `feed_name`               | `feed.name`                                |
| `feed_status`             | One of the values `"production"`, `"beta"` |
| `ip`                      | The IPv4 or IPv6 address of the event      |
| `domain`                  | The domain the event                       |


Exactly one of the parameters `ip` or `domain` must be given. The other
parameters are all required.

Example URLs:

    https://tuency-api-host/intelmq/lookup?classification_taxonomy=availability&classification_type=backdoor&feed_provider=Team+Cymru&feed_name=FTP&feed_status=production&ip=192.168.43.32
    https://tuency-api-host/intelmq/lookup?classification_taxonomy=vulnerable&classification_type=exploit&feed_provider=ShadowServer&feed_name=Phishing&feed_status=beta&domain=www.example.com


### Authentication

Authentication is done with an API key. The client should send that key
in an `Authorization` header field as a `Bearer`-token, e.g.:

    Authorization: Bearer 3JEba0eDP...

The token is configured in the backend's `.env` configuration file
with the parameter `INTELMQ_QUERY_TOKEN`.


### Lookup algorithm

1. Lookup IP address or domain name in manual data

   If found:

      - Iterate through the rules of the organisation associated with
        the IP address and domain name in ID order.If a rule matches,
        continue with "Result" below.

   Otherwise:

      - Lookup IP address and domain name in automatic data

        This can yield multiple results because the data might have been
        imported from multiple different sources.

2. Global rules

   We should only get here if either there was no manual managed network object
   or none of the associated rules matched.

   Iterate through the global rules in ID order. If a rule matches, that
   rule is the basis of the result.

3. Result

   At this point we have zero or one rule that matched (either from the
   regular rules or the global rules) and zero or more entries with
   associated information about contacts.

   The result is a JSON object described below.


### Data structure of the result


The value for each key is a JSON object with the following fields:

| Field          | Description                             |
|----------------|-----------------------------------------|
| `suppress`     | Taken from matched rule                 |
| `interval`     | Taken from matched rule. Dict with items `unit` and `length` |
| `constituencies` | A list of constituencies for the result |
| `ip`           | List of destinations for the IP address |
| `domain`       | List of destinations for the domain     |

As lookups can be performed `ip` and `domain` at once, the corresponding keys of
`ip` and `domain` in result are only present if they are in the query.
`ip` and `domain` are dictionaries with a key `destinations` which holds an
object with these fields:

| Field          | Description                                               |
|----------------|-----------------------------------------------------------|
| `source`       | String. `"portal"` or the contents of `import_source`     |
| `name`         | String. Name of the contact                               |
| `contacts`     | List of dictionaries. Only item is `email` (string value) |
| `netobject`    | The netobject matching the result (only for `ip` destination) |

### Clients

IntelMQ comes with a
[bot](https://github.com/certtools/intelmq/blob/develop/intelmq/bots/experts/tuency/expert.py)
([docs](https://intelmq.readthedocs.io/en/maintenance/user/bots.html#tuency))
that can use this API.
