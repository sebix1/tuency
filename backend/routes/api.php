<?php

/**
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020, 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 * Author: 2020 Fadi Abbud <fadi.abbud@intevation.de>
 * Author: 2021 Magnus Schieder <magnus.schieder@intevation.de>
 */

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\KeycloakUsersController;
use App\Http\Controllers\OrganisationController;
use App\Http\Controllers\OrganisationAbuseCController;
use App\Http\Controllers\TenantController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\ContactTagController;
use App\Http\Controllers\OrganisationTagController;
use App\Http\Controllers\AsnController;
use App\Http\Controllers\AsnRuleController;
use App\Http\Controllers\NetworkController;
use App\Http\Controllers\NetworkRuleController;
use App\Http\Controllers\FqdnController;
use App\Http\Controllers\FqdnRuleController;
use App\Http\Controllers\OrganisationRuleController;
use App\Http\Controllers\GlobalRuleController;
use App\Http\Controllers\MailController;
use App\Http\Controllers\NetobjectsController;
use App\Http\Controllers\NetworkAutomaticRuleController;
use App\Http\Controllers\ClassificationTaxonomyController;
use App\Http\Controllers\ClassificationTypeController;
use App\Http\Controllers\FeedProviderController;
use App\Http\Controllers\FeedNameController;
use App\Http\Controllers\EventDestinationController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\ConfigController;
use App\Http\Controllers\RipeHandleController;
use App\Http\Controllers\RipeHandleNetworkController;

/**
 * Small test to see if the server is up.
 * This API can be called unauthorized and offers only fixed content.
 */
Route::get('/uptest', function (Request $request) {
    return [
        "upTest" => "Server is up.",
    ];
});

/**
 * Send basic config and user values (to use on our SPA).
 *
 * Should usually be called only once per session.
 * Except if a value is expected to change.
 */
Route::apiResource('/config', ConfigController::class)->only(['index']);

Route::middleware('auth:api')->apiResource(
    "users",
    KeycloakUsersController::class
)->only(['index', 'store', 'show', 'update', 'destroy']);


Route::apiResource('/organisations', OrganisationController::class);

Route::apiResource(
    '/organisations.contacts',
    ContactController::class
)->scoped()->only(['index', 'store', 'show', 'update', 'destroy']);

Route::get(
    'contacts',
    [ContactController::class, 'contacts']
);

Route::apiResource(
    '/pdf',
    PDFController::class
)->only('show', 'destroy');

Route::apiResource(
    'tenants',
    TenantController::class
)->only(['index', 'show', 'update']);

Route::get(
    '/organisations/{organisation}/asns/{asn}/conflicts',
    [AsnController::class, 'conflicts']
);
Route::apiResource(
    '/organisations.asns',
    AsnController::class
)->scoped()->only(["index", "store", 'update', 'destroy']);

Route::apiResource(
    '/organisations.asns.rules',
    AsnRuleController::class
)->scoped()->only(['index', 'store', 'show', 'update', 'destroy']);

Route::get(
    '/organisations/{organisation}/networks/{network}/conflicts',
    [NetworkController::class, 'conflicts']
);

Route::apiResource(
    '/organisations.networks',
    NetworkController::class
)->scoped()->only(["index", "store", 'update', 'destroy']);

Route::apiResource(
    '/organisations.networks.rules',
    NetworkRuleController::class
)->scoped()->only(['index', 'store', 'show', 'update', 'destroy']);

Route::get(
    '/organisations/{organisation}/fqdns/{fqdn}/conflicts',
    [FqdnController::class, 'conflicts']
);
Route::apiResource(
    '/organisations.fqdns',
    FqdnController::class
)->scoped()->only(["index", "store", 'update', 'destroy']);

Route::apiResource(
    '/organisations.fqdns.rules',
    FqdnRuleController::class
)->scoped()->only(['index', 'store', 'show', 'update', 'destroy']);

Route::get(
    '/organisations/{organisation}/ripe_handles/{ripe_handle}/conflicts',
    [RipeHandleController::class, 'conflicts']
);

Route::apiResource(
    '/organisations.ripe_handles',
    RipeHandleController::class
)->scoped()->only(["index", "store", 'update', 'destroy']);

Route::apiResource(
    '/organisations.ripe_handles.ripe_organisations.networks.rules',
    NetworkAutomaticRuleController::class
)->scoped()->only(["index", "store", 'show', 'update', 'destroy']);


Route::apiResource(
    '/organisations.ripe_handles.networks',
    RipeHandleNetworkController::class
)->scoped()->only(['index']);

Route::apiResource(
    '/organisations.abuse-c',
    OrganisationAbuseCController::class
)->scoped()->only(['index']);


Route::apiResource(
    '/organisations.rules',
    OrganisationRuleController::class
)->scoped()->only(['index', 'store', 'show', 'update', 'destroy']);

Route::apiResource(
    '/global_rules',
    GlobalRuleController::class
)->scoped()->only(['index', 'store', 'show', 'update', 'destroy']);


Route::apiResource(
    '/netobjects',
    NetobjectsController::class
)->only(["index"]);


Route::apiResource(
    '/classification_taxonomies',
    ClassificationTaxonomyController::class
)->only(['index']);

Route::apiResource(
    '/classification_types',
    ClassificationTypeController::class
)->only(['index']);

Route::apiResource(
    '/feed_providers',
    FeedProviderController::class
)->only(['index']);

Route::apiResource(
    '/feed_names',
    FeedNameController::class
)->only(['index']);

Route::apiResource(
    '/contact_tags',
    ContactTagController::class
)->only(['index', 'store', 'update', 'destroy']);

Route::apiResource(
    '/organisation_tags',
    OrganisationTagController::class
)->only(['index', 'store', 'update', 'destroy']);

Route::get(
    '/intelmq/lookup',
    [EventDestinationController::class, 'lookup']
);
