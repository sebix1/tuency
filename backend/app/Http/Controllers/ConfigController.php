<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Magnus Schieder <magnus.schieder@intevation.de>
 */

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ConfigController extends Controller
{
    private $clientRealm;
    private $keycloakUrl;
    private $contactRoles;

    public function __construct()
    {
        $clientRealm = config('tuency.keycloak_admin_tuency_realm');
        if ($clientRealm === null) {
            Log::error("tuency.keycloak_admin_tuency_realm not set");
            abort(500);
        }
        $keycloakUrl = config('tuency.keycloak_url');
        if ($keycloakUrl === null) {
            Log::error("tuency.keycloak_url not set");
            abort(500);
        }

        $contactRoles = config('tuency.contact_roles');
        if ($contactRoles === null) {
            Log::error("tuency.contact_roles not set");
            abort(500);
        }
        $contactFilds = config('tuency.contact_fields');
        if ($contactFilds === null) {
            Log::error("tuency.contact_fields not set");
            abort(500);
        }

        $roleForms = [];
        foreach ($contactRoles as $roleName => $roleFilds) {
            $roleFromFields = [];
            foreach ($roleFilds as $fieldsName => $required) {
                $fields = [
                    'name' => $fieldsName,
                    'label' => $contactFilds[$fieldsName]['label'],
                    'required' => $required,
                ];
                if (array_key_exists('type', $contactFilds[$fieldsName])) {
                    $fields['type'] = $contactFilds[$fieldsName]['type'];
                }
                if (array_key_exists('hint', $contactFilds[$fieldsName])) {
                    $fields['hint'] = $contactFilds[$fieldsName]['hint'];
                }
                if (array_key_exists('dropdown', $contactFilds[$fieldsName])) {
                    $dropdownFields = [];
                    foreach ($contactFilds[$fieldsName]['dropdown'] as $value => $label) {
                        array_push(
                            $dropdownFields,
                            [
                                'label' => $label,
                                'value' => $value,
                            ]
                        );
                    }
                    $fields['dropdown'] = $dropdownFields;
                }
                array_push($roleFromFields, $fields);
            };
            $roleForm = ['name' => $roleName,
                        'fields' => $roleFromFields];
            array_push($roleForms, $roleForm);
        };

        $this->clientRealm = $clientRealm;
        $this->keycloakUrl = $keycloakUrl;
        $this->contactRoles = $roleForms;
    }

    /**
     * Return basic config and user values (to use on our SPA).
     */
    public function index()
    {
        $user = Auth::user();
        return [
            "username" => $user->getUsername(),
            "roles" => $user->getGroups()[0],
            "groups" => $user->resolveGroups(),
            "fullname" => $user->getFullname(),
            "client_id" => $user->getClientId(),
            "client_realm" => $this->clientRealm,
            "keycloak_url" => $this->keycloakUrl,
            "contact_roles" => $this->contactRoles,
        ];
    }
}
