<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Console\Commands;

use Illuminate\Support\Facades\Log;
use Scito\Laravel\Keycloak\Admin\Facades\KeycloakAdmin;

trait AccessesKeycloak
{
    private $clientRealm;
    private $clientId;
    private $clientUUID = null;

    private function initKeycloakSettings()
    {
        $clientId = config('tuency.keycloak_admin_tuency_client_id');
        if ($clientId === null) {
            Log::error("tuency.keycloak_admin_tuency_client_id not set");
            abort(500);
        }
        $clientRealm = config('tuency.keycloak_admin_tuency_realm');
        if ($clientRealm === null) {
            Log::error("tuency.keycloak_admin_tuency_realm not set");
            abort(500);
        }

        $this->clientId = $clientId;
        $this->clientRealm = $clientRealm;
    }

    private function getClientUUID()
    {
        $this->initKeycloakSettings();

        if ($this->clientUUID === null) {
            $realm = KeycloakAdmin::realm($this->clientRealm);
            foreach ($realm->clients()->all()->toArray() as $client) {
                if ($client->getClientId() === $this->clientId) {
                    $this->clientUUID = $client->getId();
                }
            }
        }

        if ($this->clientUUID === null) {
            Log::error(
                "Could not determine the UUID for clientId and realm",
                ["clientId" => $this->clientId,
                 "realm" => $this->clientRealm]
            );
            abort(500);
        }
        return $this->clientUUID;
    }

    private function openRealm()
    {
        $this->initKeycloakSettings();
        return KeycloakAdmin::realm($this->clientRealm);
    }
}
