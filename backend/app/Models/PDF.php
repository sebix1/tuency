<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Magnus Schieder <magnus.schieder@inetvation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class PDF extends Model
{
    use HasFactory;
    use HasUpdatedBy;
    use LogsChanges;

    protected $table = 'pdf';

    protected $primaryKey = 'pdf_id';

    protected $fillable = ['contact_id' ,'name', 'pdf', 'type'];
}
