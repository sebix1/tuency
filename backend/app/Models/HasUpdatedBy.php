<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Support\Facades\Auth;

trait HasUpdatedBy
{
    protected static function bootHasUpdatedBy()
    {
        if (config('tuency.set_updated_by')) {
            static::saving(function ($model) {
                // Update the updated_by column whenever the model is changed.
                $model->setUpdatedBy();
            });
        }
    }

    public function __construct($attributes = array())
    {
        parent::__construct($attributes);

        // Make the updated_by column visible to portal admins.
        // Without authentication, i.e. when Auth::user() is null, we assume
        // that the user is not a portal admin
        $user = Auth::user();
        if (!is_null($user) && $user->isPortalAdmin()) {
            $this->makeVisible('updated_by');
        } else {
            $this->makeHidden('updated_by');
        }
    }

    public function setUpdatedBy()
    {
        $user = Auth::user();
        $this->updated_by = $user->getKeycloakUserId();
    }
}
