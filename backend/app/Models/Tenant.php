<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use App\Auth\KeycloakUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tenant extends Model
{
    use LogsChanges;

    protected $table = 'tenant';

    protected $primaryKey = 'tenant_id';

    protected $fillable = ["name", "from_address", "from_name", "reply_to"];


    /**
     * Returns true if all specified TenantIds exist and the user has the rights
     * to edit the tenants.
     *
     * Portaladmins are allowed to edit all tenants.
     * Therefore, for this group it is only necessary to check if the tenants
     * exist.
     *
     * For tenantadmins must additionally check if they are admin for the
     * tenants.
     *
     * Orga-admins are not allowed to edit tenants.
     */
    public static function checkAuthorisation($user, $tenantIds)
    {
        $allowedIds = [];

        if ($user->isPortalAdmin()) {
            // For a portal admin we only need to check that the tenant
            // exists.
            $allowedIds = Tenant::whereIn('tenant_id', $tenantIds)
                ->get()->pluck('tenant_id')->toArray();
        } elseif ($user->isTenantAdmin()) {
            // For tenant admins only the tenants they're associated with are
            // allowed.
            $allowedIds = DB::table('tenant_user')
                ->where('keycloak_user_id', $user->getKeycloakUserId())
                ->whereIn('tenant_id', $tenantIds)
                ->get()
                ->pluck('tenant_id')
                ->toArray();
        }

        sort($tenantIds);
        sort($allowedIds);
        return $allowedIds === $tenantIds;
    }

    public function scopeAssignableBy($query, KeycloakUser $user)
    {
        if ($user->isPortalAdmin()) {
            return $query;
        }

        return $query->whereHas('users', function ($query) use ($user) {
            return $query->where('tenant_user.keycloak_user_id', $user->getKeycloakUserId());
        });
    }

    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'tenant_user',
            'tenant_id',
            'keycloak_user_id'
        )->using(TenantUser::class)->withTimestamps();
    }
}
