<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Magnus Schieder <magnus.schieder@intevation.de>
 */

namespace App\Models;

use App\Auth\KeycloakUser;
use App\Models\Organisation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class Contact extends Model
{
    use HasFactory;
    use HasUpdatedBy;
    use LogsChanges;

    protected $table = 'contact';

    protected $primaryKey = 'contact_id';

    protected $fillable = [
        'organisation_id',
        'roles',
    ];


    public function getFillable()
    {
        $contactFields = config('tuency.contact_fields');
        if ($contactFields === null) {
             Log::error("tuency.contact_fields not set");
             abort(500);
        }
        return array_merge(parent::getFillable(), array_keys($contactFields));
    }

    protected static function booted()
    {
        static::deleting(function ($contact) {
            // when deleting a Contact, remove associated PDFs as well.
            $contact->pdf()->delete();
            // Remove the tags, too
            $contact->tags()->sync([]);
        });
    }

    /*
     * Returns true if the user is allowed to access the contact with the given
     * contactId.
     *
     * The PortalAdmin is always allowed to access all contacts. Therefore, the
     * function returns true without further checking.
     */

    public static function checkAuthorisation($contactId)
    {
        $user = Auth::user();
        if (!$user->isPortalAdmin()) {
            $contactIds = Contact::whereIn(
                'organisation_id',
                Organisation::forUser($user)
                ->get()
                ->pluck('organisation_id')
            )->get()->pluck('contact_id')->toArray();

            if (!in_array($contactId, $contactIds)) {
                return false;
            }
        }
        return true;
    }


    public function pdf()
    {
        return $this->hasMany(PDF::class, 'contact_id');
    }

    public function organisation()
    {
        return $this->belongsTo(Organisation::class, 'organisation_id');
    }

    public function tags()
    {
        return $this->belongsToMany(
            ContactTag::class,
            'contact_contact_tag',
            'contact_id',
            'contact_tag_id',
        )->using(ContactContactTag::class)->withTimestamps();
    }

    /**
     * Scope restricting the query to contacts having a specific tag.
     */
    public function scopeForTag($query, int $tagId)
    {
        return $query->whereHas('tags', function ($query) use ($tagId) {
            return $query->where('contact_tag.contact_tag_id', $tagId);
        });
    }


    /**
     * Remove tags that the contact is not allowed to have.
     *
     * This is useful for cleanups after e.g. the set of tenants a tag or
     * organisation belongs to has been changed.
     */
    public function removeIllegalTags()
    {
        // Assignable are all tags that belong to one of the tenants the
        // contact's organisation belongs to.
        $assignableTags = ContactTag::whereHas('tenants', function ($query) {
            $query->whereIn(
                'tenant.tenant_id',
                $this->organisation->tenants()->pluck('tenant.tenant_id')
            );
        })->get();

        // The assigned tags are the tags already assigned to the contact
        $assignedTags = $this->tags;

        // The new set of tags are all assigned tags that are also assignable.
        $newTags = $assignedTags->intersect($assignableTags);

        $this->tags()->sync($newTags);
    }
}
