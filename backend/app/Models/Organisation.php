<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
 *
 * Author: 2020 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use App\Auth\KeycloakUser;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Tenant;

class Organisation extends Model
{
    use HasFactory;
    use HasUpdatedBy;
    use LogsChanges;

    // For consistency with the schema expected by the RIPE importer we
    // use the singular for the table name.
    protected $table = 'organisation';

    // For consistency with the schema expected by the RIPE importer we
    // use 'organisation_id' instead of 'id'.
    protected $primaryKey = 'organisation_id';

    protected $fillable = ["name", "parent_id"];

    protected static function booted()
    {
        static::deleting(function ($organisation) {
            // remove related data.
            $organisation->tenants()->detach();
            $organisation->users()->detach();
            $organisation->tags()->detach();
            $organisation->rules()->get()->each->delete();
            $organisation->networkAutomaticRules()->get()->each->delete();
            $organisation->asns()->get()->each->delete();
            $organisation->networks()->get()->each->delete();
            $organisation->fqdns()->get()->each->delete();
            $organisation->ripeHandles()->get()->each->delete();
            $organisation->contacts()->get()->each->delete();
        });
    }

    /**
     * Return the Organisation for an API route.
     *
     * This method assumes the user is authenticated and only returns
     * organisations that the user is allowed to work with according to the
     * forUser method.
     */
    public function resolveRouteBinding($value, $field = null)
    {
        return $this->forUser(Auth::user())->where(
            "organisation.organisation_id",
            $value
        )->firstOrFail();
    }

    /**
     * Return a query for the recursive organisation/ancestor relation
     *
     * The query is a join of the organisation table and the
     * organisation_ancestor view. The result set contains organisations
     * multiple times if they do have ancestors, and thus should usually be
     * used with e.g. the query's distinct method to make sure each
     * organisation is included only once. An alternative might be suitable
     * grouping with the groupBy method.
     */
    public static function queryAncestors()
    {
        return self::query()
                ->join(
                    'organisation_ancestor',
                    'organisation_ancestor.organisation_id',
                    '=',
                    'organisation.organisation_id'
                );
    }

    /**
     * Return a query for the organisations for a given user.
     *
     * The returned query can be further refined just as any other query. Due
     * to the joins that are done in some cases, names of columns of the
     * organisation table used in the select method should be prefixed with
     * 'organisation.'.
     *
     * The set of organisations is restricted depending on the user's group:
     *
     * portaladmin: no restriction, so forUser is almost equivalent to query,
     * except that it uses queryAncestors (see below for the rationale).
     *
     * tenantadmin: restricted to organisations that are associated with any
     * of the tenants the user is associated with.
     *
     * orgaadmin: restricted to any organisation the current user is
     * associated with
     *
     * If the user has more than one of these groups, the result set is
     * effectively the union of the above subsets. E.g. for a user who is both
     * a tenantadmin for a tentant T and an orgaadmin for an organisation O
     * associated with T and does not have any other rights, the result set
     * will consist of all organisations of T.
     *
     * This method always uses the query returned by queryAncestors as the
     * base even in cases where the hierarchy information is not directly
     * needed for the purposes of the forUser method (e.g. for a portal admin
     * we could get by without it). The advantage of always using
     * queryAncestors is that the returned query will have the same basic
     * structure in all cases and in particular will always provide access to
     * the hierarchy information in the same way. This way, callers which need
     * to restrict the query further with additional where clauses that use
     * the ancestor_id column do not have to handle portaladmins differently
     * than e.g. orgaadmins.
     *
     * Note: organisations can be included in the result set multiple times if
     * it is related to the user in multiple ways (e.g. once directly and once
     * through a direct relation of a parent organisation). See the comment
     * for queryAncestors for how handle to handle that.
     */
    public static function forUser(KeycloakUser $user)
    {
        $query = self::queryAncestors();

        // Restrictions are only necessary for users who are not portaladmins
        if (!$user->isPortalAdmin()) {
            $query = $query->where(function ($query) use ($user) {
                $query->when($user->isTenantAdmin(), function ($query) use ($user) {
                    return $query->whereExists(function ($sub) use ($user) {
                        $tenantIds = DB::table('tenant_user')
                            ->where('keycloak_user_id', $user->getKeycloakUserId())
                            ->pluck('tenant_id');
                        $sub->select(DB::raw(1))
                            ->from('organisation_tenant')
                            ->whereColumn(
                                'organisation_tenant.organisation_id',
                                '=',
                                'ancestor_id'
                            )->whereIn('tenant_id', $tenantIds);
                    });
                })->when($user->isOrgaAdmin(), function ($query) use ($user) {
                    return $query->orWhere(function ($orQuery) use ($user) {
                        $orQuery->whereExists(function ($sub) use ($user) {
                            $sub->select(DB::raw(1))
                                ->from('organisation_user')
                                ->whereColumn(
                                    'organisation_user.organisation_id',
                                    '=',
                                    'ancestor_id'
                                )->where(
                                    'keycloak_user_id',
                                    $user->getKeycloakUserId()
                                );
                        });
                    });
                });
            });
        }

        return $query;
    }

    /**
     * Create a query for a sub-hierarchy of organisations.
     *
     * This method builds on the forUser method and restricts that query
     * further.
     *
     * If the parameter ancestor is given it must be the ID of an organisation
     * and the query is restricted to that organisation and its direct and
     * indirect suborganisations.
     *
     * If the parameter 'tenant' is given it must be the ID of an tenant, and
     * the query is restricted to organisations that are associated with that
     * tenant.
     *
     * If both are given, both restrictions are applied.
     */
    public static function querySubHierarchy(
        KeycloakUser $user,
        int $ancestor = null,
        int $tenant = null
    ) {
        $query = self::forUser($user);
        if (! is_null($ancestor)) {
            $query = $query->where('ancestor_id', '=', $ancestor);
        }

        if (! is_null($tenant)) {
            $query = $query->whereExists(function ($sub) use ($tenant) {
                $sub->select(DB::raw(1))
                    ->from('organisation_tenant')
                    ->whereColumn(
                        'organisation_tenant.organisation_id',
                        '=',
                        'organisation_ancestor.ancestor_id'
                    )->where('tenant_id', '=', $tenant);
            });
        }

        return $query;
    }

    /*
    * Return the parent's names of a given organisation's id
    */
    public static function getParents(int $id)
    {
        return self::whereIn('organisation_id', function ($query) use ($id) {
            $query->select('ancestor_id')->from('organisation_ancestor')
            ->where('organisation_id', $id);
        })->pluck('name');
    }

    /** one-to-many relation ship with ASNs
     *
     * Each organisation belongs to one or more tenants.
     */
    public function tenants()
    {
        return $this->belongsToMany(
            Tenant::class,
            'organisation_tenant',
            'organisation_id',
            'tenant_id'
        )->using(OrganisationTenant::class)->withTimestamps();
    }

    /** one-to-many relation ship with users
     */
    public function users()
    {
        return $this->belongsToMany(
            User::class,
            'organisation_user',
            'organisation_id',
            'keycloak_user_id',
        )->using(OrganisationUser::class)->withTimestamps();
    }

    public function tags()
    {
        return $this->belongsToMany(
            OrganisationTag::class,
            'organisation_organisation_tag',
            'organisation_id',
            'organisation_tag_id',
        )->using(OrganisationOrganisationTag::class)->withTimestamps();
    }

    /**
     * Scope restricting the query to organisations having a specific tag.
     */
    public function scopeForTag($query, int $tagId)
    {
        return $query->whereHas('tags', function ($query) use ($tagId) {
            return $query->where('organisation_tag.organisation_tag_id', $tagId);
        });
    }

    /** one-to-many relation ship with ASNs
     *
     * Each organisation has 0 or more associated ASNs.
     */
    public function asns()
    {
        return $this->hasMany(Asn::class, 'organisation_id');
    }

    /** one-to-many relation ship with networks
     *
     * Each organisation has 0 or more associated networks.
     */
    public function networks()
    {
        return $this->hasMany(Network::class, 'organisation_id');
    }

    /** one-to-many relation ship with fqdns
     *
     * Each organisation has 0 or more associated fqdns.
     */
    public function fqdns()
    {
        return $this->hasMany(Fqdn::class, 'organisation_id');
    }

    /** one-to-many relation ship with ripe_handles
     *
     * Each organisation has 0 or more associated ripe_handles.
     */
    public function ripeHandles()
    {
        return $this->hasMany(RipeHandle::class, 'organisation_id');
    }


    /** one-to-many relation ship with approved ripe_handles
     *
     * Each organisation has 0 or more associated ripe_handles.
     */
    public function approvedRipeHandles()
    {
        return $this->ripeHandles()->where('approval', 'approved');
    }

    /** one-to-many relation ship with contacts
     *
     * Each organisation has 0 or more associated contacts.
     */
    public function contacts()
    {
        return $this->hasMany(Contact::class, 'organisation_id');
    }

    /** one-to-many relation ship with rules
     *
     * Each organisation has 0 or more associated rules.
     */
    public function rules()
    {
        return $this->hasMany(OrganisationRule::class, 'organisation_id');
    }

    /**
     * An organisation may have 0 or more associated NetworkAutomaticRules
     */
    public function networkAutomaticRules()
    {
        return $this->hasMany(NetworkAutomaticRule::class, 'organisation_id');
    }


    /**
     * Scope for querying organisations for an orgaadmin.
     *
     * This scope restricts the query to the organisations the orgaadmin is
     * directly associated with according to the organisation_user table.
     */
    public function scopeOrgaAdmin($query, $keycloakUserId)
    {
        return $query->join(
            'organisation_user',
            'organisation.organisation_id',
            '=',
            'organisation_user.organisation_id'
        )->where('keycloak_user_id', $keycloakUserId);
    }

    /**
     * Retrieve the preferred abuse-c email address from associated RIPE data
     *
     * An organisation may be associated with any number of RIPE organisations
     * via approved RIPE handles (see the approvedRipeHandles relationship).
     * Each of these RIPE organisations has an abuse-c contact. This method
     * returns the preferred one of those. Which one is considered the
     * preferred one is consistent but arbitrary (it's made consistent by
     * sorting the candidates in the database query).
     *
     * If no abuse-c address can be found, the method returns null.
     */
    public function preferredAbuseC()
    {
        $handles = $this->approvedRipeHandles()
            ->orderBy('ripe_org_hdl', 'asc')
            ->with('ripeOrganisations', function ($query) {
                return $query
                    ->orderBy('name', 'asc')
                    ->orderBy('import_source', 'asc')
                    ->with('contacts', function ($query) {
                        return $query->orderBy('email', 'asc');
                    });
            })
            ->get();

        foreach ($handles as $handle) {
            foreach ($handle->ripeOrganisations as $org) {
                foreach ($org->contacts as $contact) {
                    return $contact->email;
                }
            }
        }

        return null;
    }
    /*
     * Remove tags that the organisation is not allowed to have.
     *
     * This is useful for cleanups after e.g. the set of tenants a tag or
     * organisation belongs to has been changed.
     */
    public function removeIllegalTags()
    {
        // Assignable are all tags that belong to one of the tenants the
        // contact's organisation belongs to.
        $assignableTags = OrganisationTag::whereHas('tenants', function ($query) {
            $query->whereIn(
                'tenant.tenant_id',
                $this->tenants()->pluck('tenant.tenant_id')
            );
        })->get();

        // The assigned tags are the tags already assigned to the contact
        $assignedTags = $this->tags;

        // The new set of tags are all assigned tags that are also assignable.
        $newTags = $assignedTags->intersect($assignableTags);

        $this->tags()->sync($newTags);
    }
}
