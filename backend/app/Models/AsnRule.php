<?php

/*
 * This file is Free Software under GNU Affero General Public License v >= 3.0
 * without warranty, see README.md and license for details.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *
 * SPDX-FileCopyrightText: 2021 nic.at GmbH <https://nic.at>
 * Software-Engineering: 2021 Intevation GmbH <https://intevation.de>
 *
 * Author: 2021 Bernhard Herzog <bernhard.herzog@intevation.de>
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use App\Models\Asn;
use App\Models\Rule;

class AsnRule extends Rule
{
    use HasFactory;

    protected $table = 'asn_rule';

    protected $primaryKey = 'asn_rule_id';

    protected $parent_id_column = 'asn_id';

    protected static function booted()
    {
        static::deleting(function ($rule) {
            // when deleting an AsnRule, remove the association with the
            // contacts, too, but not the contacts themselves.
            $rule->contacts()->sync([]);
        });
    }

    /** one-to-many relation ship with contacts
     *
     * Each rules has 0 or more associated contacts
     */
    public function contacts()
    {
        return $this->belongsToMany(
            Contact::class,
            'asn_rule_contact',
            'asn_rule_id',
            'contact_id'
        )->withTimestamps();
    }
}
