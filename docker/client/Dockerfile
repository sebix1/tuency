FROM debian:bullseye

EXPOSE 80 7080

LABEL maintainer="awoestmann@intevation.de"

ENV LUA_LIB=/usr/local/lib/lua/5.1/
ENV LUA_INC=/usr/local/include/
ENV OPENRESTY_PATH=/usr/local/openresty
ENV VUE_BACKEND_API_URL=http://tuency_client:80/api
ENV VUE_APP_KEYCLOAK_URL=http://tuency_keycloak:8080
ENV VUE_APP_DISABLE_HOST_CHECK=true
ENV DEBIAN_FRONTEND noninteractive

ADD client /opt/tuency_client
ADD docker/client /opt/tuency_conf
ADD docs/examples/tuency_resources /opt/tuency_resources
ADD docs/examples /opt/tuency_scripts
WORKDIR /opt/tuency_conf

# Install php and extensions required by laravel
RUN apt update && apt install -y wget curl gnupg

# yarn requires node >= 16, we use the current version 18
# Install node as documented upstream https://github.com/nodesource/distributions/blob/master/README.md#debmanual
ENV KEYRING=/etc/apt/trusted.gpg.d/nodesource.gpg
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource.gpg.key | gpg --dearmor | tee "$KEYRING" >/dev/null
RUN echo "deb [signed-by=$KEYRING] https://deb.nodesource.com/node_18.x bullseye main" | tee /etc/apt/sources.list.d/nodesource.list

# install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add - \
    && echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list \
    && apt -y update \
    && apt -y install yarn

RUN ./installOpenIdc.sh

RUN mkdir ${OPENRESTY_PATH}/nginx/sites \
    && ln -sf /opt/tuency_conf/nginx.conf ${OPENRESTY_PATH}/nginx/conf \
    && ln -s /opt/tuency_conf/tuency_client.local ${OPENRESTY_PATH}/nginx/sites/tuency_client.local \
    && ln -s /opt/tuency_resources ${OPENRESTY_PATH}/nginx/tuency_resources \
    && ln -s /opt/tuency_conf/.env /opt/tuency_client/.env

RUN mkdir /var/log/openresty \
    && touch /var/log/openresty/access.log \
    && touch /var/log/openresty/error.log

RUN echo -e "127.0.0.1\ttuencyone tuencytwo tuencythree" > /etc/hosts

CMD ["/opt/tuency_conf/start.sh"]
