#!/bin/bash

set -eu -o pipefail
test -n "${DEBUG-}" && set -x

LUA_VERSION=5.1.5
LUAROCKS_VERSION=3.3.1

apt -y install --no-install-recommends wget gnupg ca-certificates unzip build-essential libreadline-dev
wget -qO - https://openresty.org/package/pubkey.gpg | apt-key add -
echo "deb http://openresty.org/package/debian buster openresty" \
    | tee /etc/apt/sources.list.d/openresty.list
apt update
apt -y install openresty

#Install lua
curl -R -O http://www.lua.org/ftp/lua-$LUA_VERSION.tar.gz  > /dev/null
tar -zxf lua-$LUA_VERSION.tar.gz  > /dev/null
cd lua-$LUA_VERSION
make linux test > /dev/null
make install > /dev/null
cd ..


#Install lua rocks
curl -L https://luarocks.org/releases/luarocks-$LUAROCKS_VERSION.tar.gz -o luarocks-$LUAROCKS_VERSION.tar.gz > /dev/null
tar zxpf luarocks-$LUAROCKS_VERSION.tar.gz > /dev/null
cd luarocks-$LUAROCKS_VERSION
./configure --with-lua-include=/usr/local/include > /dev/null
make > /dev/null
make install > /dev/null
cd ..

luarocks install lua-cjson
luarocks install lua-resty-openidc 1.7.6-3
