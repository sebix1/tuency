#!/bin/bash

echo "###Setting up database###"

set -eu -o pipefail
test -n "${DEBUG-}" && set -x

db_name=tuencydb
user=tuency
password=secret
psql --command "CREATE DATABASE $db_name;" >> /dev/null
#Create user and assign roles
psql -d $db_name --command "CREATE USER $user with password '$password';" >> /dev/null
psql -d $db_name --command "ALTER USER $user with superuser;" >> /dev/null
psql -d $db_name --command "ALTER USER postgres with password '$password';" >> /dev/null
echo "Database: $db_name"
echo "Example user(password): $user($password)"
