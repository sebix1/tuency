#!/bin/sh -e

DIR=$(dirname $0)
FILE="${DIR}/keycloak.configured"

if test -f "$FILE"; then
    echo "Keycloak seems to already been set up."
    exit
fi

wait-for-it localhost:8080 -t 120 || { echo "Failed starting Keycloak"; exit 1; }

#Try to get access token
KEYCLOAK_URL=http://localhost:8080/auth
KEYCLOAK_REALM=master

KEYCLOAK_ADMIN=admin
KEYCLOAK_ADMIN_PASSWORD=secret

TUENCY_REALM=master

TKN="null"
counter=1
max_retry=10

while [ "$TKN" = "null" -a "$counter" != "$max_retry" ]
do
    echo "Trying to get acess token..."
    TKN=$(curl -sX POST "${KEYCLOAK_URL}/realms/${KEYCLOAK_REALM}/protocol/openid-connect/token" \
    -H "Content-Type: application/x-www-form-urlencoded" \
    -d "username=${KEYCLOAK_ADMIN}" \
    -d "password=${KEYCLOAK_ADMIN_PASSWORD}" \
    -d 'grant_type=password' \
    -d 'client_id=admin-cli' | jq -r '.access_token')
    counter=$((counter+1))
    sleep 2
done

#Create clients

for clientjson in "tuency_client.json" "tuencyone.json" "tuencytwo.json" "tuencythree.json"
do
    echo "Creating tuency_client: $TUENCY_REALM->$(jq .clientId ${DIR}/${clientjson})"
    curl -sX POST "${KEYCLOAK_URL}/admin/realms/$TUENCY_REALM/clients" \
        -H "Content-Type: application/json" \
        -H "Authorization: Bearer $TKN" \
        -d @${DIR}/${clientjson}
done

#Create client roles
TUENCY_CLIENT_ID=$(jq .id ${DIR}/tuency_client.json | tr -d '"')
TUENCY_ONE_ID=$(jq .id ${DIR}/tuencyone.json | tr -d '"')
TUENCY_TWO_ID=$(jq .id ${DIR}/tuencytwo.json | tr -d '"')
TUENCY_THREE_ID=$(jq .id ${DIR}/tuencythree.json | tr -d '"')

users=$(curl -sX GET "${KEYCLOAK_URL}/admin/realms/$TUENCY_REALM/users" \
        -H "Content-Type: application/json" \
        -H "Authorization: Bearer $TKN")
userId=$(echo $users | jq 'map(select(.username=="admin")) | .[0]' | jq .id | tr -d '"')

echo "Creating and assigning portaladmin roles"
for clientid in "${TUENCY_CLIENT_ID}" "${TUENCY_ONE_ID}" "${TUENCY_TWO_ID}" "${TUENCY_THREE_ID}"
do
    curl -sX POST "${KEYCLOAK_URL}/admin/realms/$TUENCY_REALM/clients/$clientid/roles" \
        -H "Content-Type: application/json" \
        -H "Authorization: Bearer $TKN" \
        -d "{\"name\": \"portaladmin\"}"
    role=$(curl -sX GET "${KEYCLOAK_URL}/admin/realms/$TUENCY_REALM/clients/$clientid/roles" \
        -H "Content-Type: application/json" \
        -H "Authorization: Bearer $TKN")
    curl -sX POST "${KEYCLOAK_URL}/admin/realms/$TUENCY_REALM/users/$userId/role-mappings/clients/$clientid" \
        -H "Content-Type: application/json" \
        -H "Authorization: Bearer $TKN" \
        -d "$role"
done

echo "Creating roles tenantadmin and orgaadmin"
for role in "tenantadmin" "orgaadmin";
    do
    curl -sX POST "${KEYCLOAK_URL}/admin/realms/$TUENCY_REALM/clients/$TUENCY_CLIENT_ID/roles" \
        -H "Content-Type: application/json" \
        -H "Authorization: Bearer $TKN" \
        -d "{\"name\": \"$role\"}"
done

touch $FILE
