--[[
    Check authentication for the given request
    Uses the following nginx variables:
        -client_id: KeyCloak client id
        -redirect_uri: Redirect uri after logout. Will also be send to the backend
        -keycloak_url: URL to the keycloak instance
        -keycloak_realm: The realm to use for authentication
]]
local opts = {
    redirect_uri = "/redirect",
    accept_none_alg = true,
    discovery =  ngx.var.keycloak_url .. "/auth/realms/" .. ngx.var.keycloak_realm .. "/.well-known/openid-configuration",
    client_id = ngx.var.client_id,
    logout_path = "/logout",
    redirect_after_logout_uri =  ngx.var.keycloak_url .. "/auth/realms/" .. ngx.var.keycloak_realm .. "/protocol/openid-connect/logout?redirect_uri=" .. ngx.var.redirect_uri,
    redirect_after_logout_with_id_token_hint = false,
    session_contents = {id_token=true, access_token=true},
    introspection_cache_ignore = true,
    -- disable certification checking for non production setups
    ssl_verify = "no"
}

local res, err = require("resty.openidc").authenticate(opts, nil, "pass")
if err or not res then
    ngx.status = 401
    ngx.exit(ngx.HTTP_UNAUTHORIZED)
else
    --Append user info to request
    ngx.req.set_header("X-SUB", res.id_token.sub)
    ngx.req.set_header("x-user-id-username", res.id_token.username)
    ngx.req.set_header("x-user-id-group",
            table.concat(res.id_token.clientroles, ","))
    ngx.req.set_header("x-user-id-fullname", res.id_token.name)
    ngx.req.set_header("x-keycloak-client_id", ngx.var.client_id)
    ngx.req.set_header("x-redirect-uri", ngx.var.redirect_uri)

    --Enable debug logging
    local openidc = require("resty.openidc")
    openidc.set_logging(nil, { DEBUG = ngx.INFO })
end

