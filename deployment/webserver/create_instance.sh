#!/bin/bash
#Script to set up a tuency web application instance by configuring and deploying a server block to OpenResty

#Paths
CLIENT_DIR=/usr/local/tuency
INSTALL_DIR=/usr/local/openresty/nginx/sites
SCRIPTS_DIR=/usr/local/openresty/nginx/scripts

#Connection data
BACKEND_URL=""
KEYCLOAK_URL=""
PORT=80

CLIENT_NAME=""
RESOURCE_DIR=""
SERVER_NAME=""
SHARED_RESOURCE_DIR=""

printUsage()
{
    echo "Usage: $0 -c CLIENT_NAME -r RESOURCE_DIR -s SERVER_NAME --backend-url BACKEND_URL --keycloak-url KEYCLOAK_URL [-p PORT] [-h] [--client-dir CLIENT_DIR] [--install-dir INSTALL_DIR] [--scripts-dir SCRIPTS_DIR] [--shared-resource-dir SHARED_RESOURCE_DIR]"
    echo -e "\t-c Set the client name to use for the instance, mandatory"
    echo -e "\t-r Set the resource directory to use for the instance, mandatory"
    echo -e "\t-s Set the server name to use for the instance, mandatory"
    echo -e "\t-p Set the port to use for the instance, defaults to 80"
    echo -e "\t-h Show this usage information"
    echo -e "\t--backend-url URL to tuency backend, mandatory"
    echo -e "\t--keycloak-url URL to the keycloak instance, mandatory"
    echo -e "\t--client-dir Set the directory the built tuency client application is present, defaults to /usr/local/tuency"
    echo -e "\t--install-dir Set the directory the configured server block will be installed to, defaults to /usr/local/openresty/nginx/sites"
    echo -e "\t--scripts-dir Set the directory where the tuency lua scripts are present, defaults to /usr/local/openresty/nginx/scripts"
    echo -e "\t--shared-resource-dir Set the directory where shared resources can be found, defaults to {resource-dir}/logo"
    exit 1 # Exit script after printing help
}

options=$(getopt -o c:r:s:p:h --long backend-url: --long keycloak-url: --long client-dir: --long install-dir: --long scripts-dir:  --long shared-resource-dir: -- "$@")
[ $? -eq 0 ] || {
    printUsage
}

eval set -- "$options"

while true; do
    case "$1" in
    -c)
        shift;
        CLIENT_NAME=$1 ;;
    -r)
        shift;
        RESOURCE_DIR=$1 ;;
    -s)
        shift;
        SERVER_NAME=$1 ;;
    -p)
        shift;
        PORT=$1 ;;
    -h) printUsage ;;
    --backend-url)
        shift;
        BACKEND_URL=$1
        ;;
    --keycloak-url)
         shift;
         KEYCLOAK_URL=$1
         ;;
    --client-dir)
        shift;
        CLIENT_DIR=$1
        ;;
    --install-dir)
        shift;
        INSTALL_DIR=$1
        ;;
    --scripts-dir)
        shift;
        SCRIPTS_DIR=$1
        ;;
    --shared-resource-dir)
        shift;
        SHARED_RESOURCE_DIR=$1
        ;;
    --)
        shift
        break
        ;;
    esac
    shift
done

if [ -z $CLIENT_NAME ]
    then echo "No client name specified. Use the -c parameter to set one."
    printUsage
fi

if [ -z $RESOURCE_DIR ]
    then echo "No resource directory specified. Use the -r parameter to set one."
    printUsage
fi

if [ -z $SERVER_NAME ]
    then echo "No server name specified. Use the -s parameter to set one."
    printUsage
fi

if [ -z $SHARED_RESOURCE_DIR ]
    then SHARED_RESOURCE_DIR="$RESOURCE_DIR/logo"
fi

SERVER_BLOCK=$INSTALL_DIR/$CLIENT_NAME

echo "Creating server config at $SERVER_BLOCK"
echo "Client name: $CLIENT_NAME"
echo "Server name: $SERVER_NAME"
echo "Resource dir: $RESOURCE_DIR"
echo "Shared resource dir: $SHARED_RESOURCE_DIR"

cp tuency_server_block $SERVER_BLOCK
sed -i 's,\$BACKEND_URL,'"$BACKEND_URL"',g' $SERVER_BLOCK
sed -i 's,\$CLIENT_DIR,'"$CLIENT_DIR"',g' $SERVER_BLOCK
sed -i 's,\$CLIENT_NAME,'"$CLIENT_NAME"',g' $SERVER_BLOCK
sed -i 's,\$KEYCLOAK_URL,'"$KEYCLOAK_URL"',g' $SERVER_BLOCK
sed -i 's,\$PORT,'"$PORT"',g' $SERVER_BLOCK
sed -i 's,\$RESOURCE_DIR,'"$RESOURCE_DIR"',g' $SERVER_BLOCK
sed -i 's,\$SERVER_NAME,'"$SERVER_NAME"',g' $SERVER_BLOCK
sed -i 's,\$SCRIPTS_DIR,'"$SCRIPTS_DIR"',g' $SERVER_BLOCK
sed -i 's,\$SHARED_RESOURCE_DIR,'"$SHARED_RESOURCE_DIR"',g' $SERVER_BLOCK
