#!/bin/bash
#Install an openresty webserver to serve the tuency client application

export DEBIAN_FRONTEND=noninteractive
PARAMETER_Y=0

OPENRESTY_REPO=http://openresty.org/package/debian
YARN_REPO=https://dl.yarnpkg.com/debian/

RESOLVER="local=on"
INCLUDE_DIR=/usr/local/openresty/nginx/sites
NGINX_CONF=/usr/local/openresty/nginx/conf/nginx.conf
SCRIPTS_DIR=/usr/local/openresty/nginx/scripts

PROJECT_ROOT=$PWD/../..
INSTALL_DIR=/usr/local/tuency

LUA_VERSION=5.3.5
LUAROCKS_VERSION=3.3.1

printUsage()
{
    echo "Usage: $0 [-y] [-h] [--install-dir INSTALL_DIR] [--include-dir INCLUDE_DIR] [--resolver RESOLVER] [--scripts-dir SCRIPTS_DIR]"
    echo -e "\t-y Assume yes for all prompts and run non-interactively"
    echo -e "\t-h Show this usage information"
    echo -e "\t--install-dir Set the directory the compiled tuency application is installed to, defaults to /usr/local/tuency"
    echo -e "\t--include-dir Set the directory containing the tuency server blocks, defaults to /usr/local/openresty/nginx/sites"
    echo -e "\t--resolver Set the resolver used by OpenResty, defaults to local"
    echo -e "\t--scripts-dir Set the directory where the tuency lua scripts will be placed"
    exit 1 # Exit script after printing help
}

options=$(getopt -o yh --long install-dir: --long include-dir: --long resolver: --long scripts-dir: -- "$@")
[ $? -eq 0 ] || {
    printUsage
}

eval set -- "$options"

while true; do
    case "$1" in
    -y) PARAMETER_Y=1 ;;
    -h) printUsage ;;
    --install-dir)
        shift;
        INSTALL_DIR=$1
        ;;
    --include-dir)
        shift;
        INCLUDE_DIR=$1
        ;;
    --resolver)
        shift;
        RESOLVER=$1
        ;;
    --scripts-dir)
        shift;
        SCRIPTS_DIR=$1
        ;;
    --)
        shift
        break
        ;;
    esac
    shift
done

if [ "$EUID" -ne 0 ]
  then echo "The setup needs root privileges to install the client dependencies. Please run as root."
  exit 1
fi

DEPENDENCIES="ca-certificates curl git gnupg unzip"

echo "Updating package information..."
apt-get update > /dev/null

echo "Installing backend dependencies.."

if [ "$PARAMETER_Y" -ne 0 ]
    then
    echo "The following packages will be installed: $DEPENDENCIES"
    apt-get install $DEPENDENCIES -y -qq > /dev/null

    else
    apt-get install $DEPENDENCIES
fi

APT_RESULT=$?

if [ "$APT_RESULT" -ne 0 ]
    then echo "Error: Dependency setup failed"
    exit 1
fi

echo "Installing OpenResty"

echo "Adding OpenResty repository: $OPENRESTY_REPO"
curl -sS https://openresty.org/package/pubkey.gpg | apt-key add -
echo "deb http://openresty.org/package/debian buster openresty" \
    | tee /etc/apt/sources.list.d/openresty.list
apt-get update > /dev/null

if [ "$PARAMETER_Y" -ne 0 ]
    then
    apt-get install openresty -y -qq > /dev/null
    else
    apt-get install openresty
fi

APT_RESULT=$?

if [ "$APT_RESULT" -ne 0 ]
    then echo "Error: OpenResty setup failed"
    exit 1
fi

echo "Installing luarocks $LUAROCKS_VERSION..."

if [ "$PARAMETER_Y" -ne 0 ]
    then
    apt-get install build-essential libreadline-dev -y -qq > /dev/null
    else
    apt-get install build-essential libreadline-dev
fi

#Install lua
curl -R -O http://www.lua.org/ftp/lua-$LUA_VERSION.tar.gz  > /dev/null
tar -zxf lua-$LUA_VERSION.tar.gz  > /dev/null
cd lua-$LUA_VERSION
make linux test > /dev/null 
make install > /dev/null
cd ..


#Install lua rocks
curl -L https://luarocks.org/releases/luarocks-$LUAROCKS_VERSION.tar.gz -o luarocks-$LUAROCKS_VERSION.tar.gz > /dev/null
tar zxpf luarocks-$LUAROCKS_VERSION.tar.gz > /dev/null
cd luarocks-$LUAROCKS_VERSION
./configure --with-lua-include=/usr/local/include > /dev/null
make > /dev/null
make install > /dev/null
cd ..

#Clean up
rm -rf lua-$LUA_VERSION lua-$LUA_VERSION.tar.gz
rm -rf luarocks-$LUAROCKS_VERSION.tar.gz luarocks-$LUAROCKS_VERSION

echo "Installing lua-resty-openidc using luarocks..."

#Install lua dependencies
luarocks install lua-cjson > /dev/null
luarocks install lua-resty-openidc 1.7.3 > /dev/null

echo "Configuring OpenResty..."
echo "Using nginx config: $NGINX_CONF"
echo "Using include dir: $INCLUDE_DIR"
echo "Using resolver: $RESOLVER"
echo "Placing scripts in: $SCRIPTS_DIR"
mkdir -p $INCLUDE_DIR
mkdir -p $SCRIPTS_DIR
cp nginx.conf $NGINX_CONF
cp authenticate.lua check_auth.lua refresh.lua $SCRIPTS_DIR
sed -i 's,\$RESOLVER,'"$RESOLVER"',g' $NGINX_CONF
sed -i 's,\$INCLUDE_DIR,'"$INCLUDE_DIR"',g' $NGINX_CONF
