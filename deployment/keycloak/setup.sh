#!/bin/bash
export DEBIAN_FRONTEND=noninteractive

INSTALL_DIR=/opt
KEYCLOAK_VERSION=11.0.3
PARAMETER_Y=0

PROJECT_ROOT=$PWD/../..
THEME_DIR=$PROJECT_ROOT/keycloak_theme/tuency_kc_theme
API_EXTENSION=""
LOGGING_EXTENSION=""

printUsage()
{
    echo "Usage: $0 --api-extension API_EXTENSION [-y] [-h] [--install-dir INSTALL_DIR] [--theme-dir THEME_DIR]"
    echo -e "\t--api-extension Api extension jar file, mandatory"
    echo -e "\t--logging-extension Logging extension jar file, mandatory"
    echo -e "\t-y Assume yes for all prompts and run non-interactively"
    echo -e "\t-h Show this usage information"
    echo -e "\t--install-dir Directory to install keycloak to. Defaults to /opt. Note: a KeyCloak subdirectory will be created"
    echo -e "\t--theme-dir Directory containing the KeyCloak theme, defaults to ../../keycloak_theme/tuency_kc_theme"
    exit 1
}

options=$(getopt -o yh --long logging-extension: --long api-extension: --long install-dir: --long theme-dir: -- "$@")
[ $? -eq 0 ] || {
    printUsage
}

eval set -- "$options"

while true; do
   case "$1" in
        -y ) PARAMETER_Y=1 ;;
        -h ) printUsage ;;
        --api-extension)
            shift;
            API_EXTENSION=$1
            ;;
        --logging-extension)
            shift;
            LOGGING_EXTENSION=$1
            ;;
        --install-dir)
            shift;
            INSTALL_DIR=$1
            ;;
        --theme-dir)
            shift;
            THEME_DIR=$1
            ;;
        --)
            shift
            break
            ;;
   esac
   shift
done

if [ -z "$API_EXTENSION" ]
    then echo "Error: No api extension jar specified"
    printUsage
fi

echo "Updating package information..."

apt-get update > /dev/null

echo "Installing backend dependencies.."

DEPENDENCIES="curl openjdk-11-jdk-headless"

if [ "$PARAMETER_Y" -ne 0 ]
    then
    echo "The following packages will be installed: $DEPENDENCIES"
    apt-get install $DEPENDENCIES -y -qq > /dev/null
    else
    apt-get install $DEPENDENCIES
fi

echo "Installing KeyCloak to $INSTALL_DIR"

mkdir -p $INSTALL_DIR
curl -L https://downloads.jboss.org/keycloak/$KEYCLOAK_VERSION/keycloak-$KEYCLOAK_VERSION.tar.gz -o keycloak-$KEYCLOAK_VERSION.tar.gz
EXPECTED_KEYCLOAK_CHECKSUM="$(curl https://downloads.jboss.org/keycloak/$KEYCLOAK_VERSION/keycloak-$KEYCLOAK_VERSION.tar.gz.sha1)  keycloak-$KEYCLOAK_VERSION.tar.gz"
echo "$EXPECTED_KEYCLOAK_CHECKSUM"
ACTUAL_KEYCLOAK_CHECKSUM=$(sha1sum keycloak-$KEYCLOAK_VERSION.tar.gz)
echo "$ACTUAL_KEYCLOAK_CHECKSUM"

if [ "$EXPECTED_KEYCLOAK_CHECKSUM" != "$ACTUAL_KEYCLOAK_CHECKSUM" ]
then
    >&2 echo "ERROR: Invalid keycloak installer checksum"
    rm keycloak-$KEYCLOAK_VERSION.tar.gz
    exit 1
fi

echo "Extracting KeyCloak"
tar -xzf keycloak-$KEYCLOAK_VERSION.tar.gz > /dev/null
rm keycloak-$KEYCLOAK_VERSION.tar.gz
mv keycloak-$KEYCLOAK_VERSION $INSTALL_DIR
MV_RESULT=$?

if [ "$MV_RESULT" -ne 0 ]
    then echo "Error: Installing KeyCloak failed"
    exit 1
fi

echo "Keycloak installed to $INSTALL_DIR/keycloak-$KEYCLOAK_VERSION"

echo "Adding keycloak theme"
cp -r $THEME_DIR $INSTALL_DIR/keycloak-$KEYCLOAK_VERSION/themes

echo "Adding API extension"
cp $API_EXTENSION $INSTALL_DIR/keycloak-$KEYCLOAK_VERSION/standalone/deployments/

echo "Adding Logging extension"
cp $LOGGING_EXTENSION $INSTALL_DIR/keycloak-$KEYCLOAK_VERSION/standalone/deployments/
