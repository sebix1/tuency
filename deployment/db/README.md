# Database

This folder contains scripts to install the tuency postgresql database including the necessary dependencies.

## Setup

To set up the database the setup.sh shell script can be used. This will install a postgresql database and its dependencies.

For usage information use:

```bash
cd /path/to/tuency/repo/deployment/db
./setup.sh -h
# Usage: ./setup.sh [-y] [-h]
#   -y Assume yes for all prompts and run non-interactively
#   -h Show this usage information
```

To install the database use:

```bash
cd /path/to/tuency/repo/deployment/db
./setup.sh
```

## Update

Database updates are applied using the backend application. Please refer to the [backend documentation](/deployment/backend/README.md)

## Docker Setup

The [Dockerfile](Dockerfile) can be used to set up a container providing the database:

Build image

```bash
cd /path/to/tuency/repo
docker build -t tuency/db -f deployment/db/Dockerfile .
```

Run container:

```bash
docker run --name tuency_db -d tuency/db
```

Further configuration can be done by connecting to the container:

```bash
#Run bash as root
docker exec -u0 -ti tuency_db bash
#Run psql as postgres
docker exec -ti tuency_db psql
```
