# Instructions

Copy (no symlink) this theme [tuency_kc_theme](tuency_kc_theme)
into the KeyCloak `themes` directory and
select it in the admin interface under //Realm Settings -> Themes// for
 * //Login//
 * //Account//

See the KeyCloak documentation for detailed instructions:
https://www.keycloak.org/docs/latest/server_development/index.html#_themes

The javascript in the themes calls `http://<host>/resources/account_style.css`
and `http://<host>/resources/login_style.css` to load host specific css.
For this the KeyCloak api must be extended by this [endpoint](keycloak_api/README.md).

Examples are available at
[../docs/examples/tuency_resources](../docs/examples/tuency_resources).
