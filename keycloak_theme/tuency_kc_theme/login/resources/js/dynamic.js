// This file is Free Software under GNU Affero General Public License v >= 3.0
// without warranty, see README.md and license for details.
//
// SPDX-License-Identifier: AGPL-3.0-or-later
//
// SPDX-FileCopyrightText: 2020 nic.at GmbH <https://nic.at>
// Software-Engineering: 2020 Intevation GmbH <https://intevation.de>
//
// Author: 2020 Magnus Schieder <magnus.schieder@intevation.de>


var client_id = new URLSearchParams(location.search).get('client_id')


if (client_id != 'security-admin-console' && client_id != null) {
    // Get the client root URL from the Keycloak API.
    const request = new Request(location.origin +
                                '/auth/realms/tuency/clienturlprovider?clientid=' +
                                client_id)

    fetch(request)
        .then(response => {
            if (response.status === 200) {
                      return response.json();
            } else {
                      throw new Error('Could not retrieve the client root Url ' +
                                      'from the Keycloak API!');
            }
        })
        .then(response => {
            // Set client specific css.
            var href = response.rootUrl;
            if (href.charAt(href.length -1) != '/') {
                href += '/';
            }
            href += 'resources/login_style.css'
            var head = document.getElementsByTagName('HEAD')[0];
            var link = document.createElement('link');
            link.rel = 'stylesheet';
            link.type = 'text/css';
            link.href = href;
            head.appendChild(link);
        }).catch(error => {
            console.error(error);
        });
}
