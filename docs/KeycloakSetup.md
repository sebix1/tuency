# KeyCloak Setup

Tuency uses KeyCloak for user authentication.
For an development environment it can be deployed using the standalone bundle, which includes the required application server.

## Installation

```bash
# If not already present, install OpenJDK > 8.
# This example uses the apt package manager and was tested on Debian Buster.
apt install openjdk-11-jdk-headless

#with a user account
#Download KeyCloak
curl https://downloads.jboss.org/keycloak/11.0.3/keycloak-11.0.3.tar.gz -o keycloak-11.0.3.tar.gz

#The archive checksum can be downloaded and checked using sha1sum or similar tools
curl https://downloads.jboss.org/keycloak/11.0.3/keycloak-11.0.3.tar.gz.sha1 -o keycloak-11.0.3.tar.gz.sha1
echo -n " keycloak-11.0.3.tar.gz" >> keycloak-11.0.3.tar.gz.sha1
sha1sum -c keycloak-11.0.3.tar.gz.sha1
sha256sum keycloak-11.0.3.tar.gz
# c05c508f81e3c749e722ce0d1e81085634521bba5a109cdca998899bad4bbe4f  keycloak-11.0.3.tar.gz


#Extract archive
tar -xvf keycloak-11.0.3.tar.gz

#Add initial admin user using the add-user shell script.
#Provide a username using the -u parameter.
#The password can be set using the -p parameter or interactively if the parameter is omitted
cd keycloak-11.0.3/bin
./add-user-keycloak.sh -u admin -p secret

#Start the wildfly application server, changing the default port to 9080
cd keycloak-11.0.3/bin
./standalone.sh -b 0.0.0.0 -bmanagement=0.0.0.0 -Djboss.http.port=9080
```

KeyCloak provides a web interface, that can then be reached using the address localhost:9080 and the credentials used in the example above.
The interface can be used to further configure KeyCloak and add an initial set of users.

Further port configuration can be done by editing the socket-binding-group in the keycloak-11.0.3/standalone/configuration/standalone.xml file.

```xml
<socket-binding-group name="standard-sockets" default-interface="public" port-offset="${jboss.socket.binding.port-offset:0}">
    <socket-binding name="management-http" interface="management" port="${jboss.management.http.port:9990}"/>
    <socket-binding name="management-https" interface="management" port="${jboss.management.https.port:9993}"/>
    <socket-binding name="ajp" port="${jboss.ajp.port:8009}"/>
    <socket-binding name="http" port="${jboss.http.port:8080}"/>
    <socket-binding name="https" port="${jboss.https.port:8443}"/>
    <socket-binding name="txn-recovery-environment" port="4712"/>
    <socket-binding name="txn-status-manager" port="4713"/>
    <outbound-socket-binding name="mail-smtp">
        <remote-destination host="localhost" port="25"/>
    </outbound-socket-binding>
</socket-binding-group>

```

If you want to setup keycloak behind a proxy, follow the
[keycloak instructions for proxy-setups](https://www.keycloak.org/docs/latest/server_installation/index.html#_setting-up-a-load-balancer-or-proxy).

Refer to the [KeyCloak Documentation](https://www.keycloak.org/docs/latest/) for further information on setting up and configuring KeyCloak.

## Configuration and client setup

### Set up a OpenID Relying Party

Tuency uses [OpenResty](https://github.com/openresty/openresty) and [lua-resty-openidc](https://github.com/zmartzone/lua-resty-openidc) to connect the client application to keycloak.

#### OpenResty installation on debian buster

The openresty Debian package already includes an `nginx` binary
and will register it to start by default.

```bash
apt -y install --no-install-recommends gnupg ca-certificates
curl https://openresty.org/package/pubkey.gpg >openresty_pubkey-`date -I`.gpg
apt-key add openresty_pubkey-`date -I`.gpg

echo "deb http://openresty.org/package/debian buster openresty" \
    | tee /etc/apt/sources.list.d/openresty.list
apt update
apt -y install openresty
```

#### nginx Configuration

[docs/examples/nginx.conf](./examples/nginx.conf) contains an example nginx config which can be placed at `/usr/local/openresty/nginx/conf/nginx.conf`.

Also create the necessary directories:
```bash
sudo mkdir /var/log/openresty/ /usr/local/openresty/nginx/sites/
```

#### Install lua-resty-openidc

Lua-resty-openidc and its dependencies can be installed using the luarocks package manager.
To install the current version of luarocks, please refer to the [lualocks github page](https://github.com/luarocks/luarocks/wiki/Installation-instructions-for-Unix).
Make sure to install the same lua version as openresty uses. You can find the version number in `/usr/local/openresty/luajit/lib/lua/`.

If an older version of luarocks is already installed, e.g., via a package manager, it is advised to uninstall this first:

```bash
apt remove luarocks
apt autoremove
```

After installing luarocks, lua-resty-openidc can be installed using:

```bash
luarocks install lua-resty-openidc 1.7.3
```

#### Server block configuration

To enable lua-resty-openidc, an access_by_lua block script block needs to be inserted into the server block.
[docs/examples/server_block](./examples/server_block) contains two example server block configurations which can be placed at `/usr/local/openresty/nginx/sites/`.
Both server blocks serve a different tuency variant using separate resource directories. [docs/examples/tuency_resources](./examples/tuency_resources) provides an example for the required directory structure.

The server block example assumes that the nginx can be reached under "http://tuencyone" and "http://tuencytwo", the KeyCloak can be reached under "http://tuency_keycloak" and the backend application can be reached under "http://tuency_backend".
These URLs may need to be modified depending on the actual development environment.

#### Controlling openresty's http server

```bash
systemctl status openresty
systemctl reload openresty
# the directly binary is
openresty -h
```

### Create a KeyCloak realm and client

Navigate to [http://localhost:9080/auth/admin/](http://localhost:9080/auth/admin/) and log in with the credentials created previously (user `admin` and password `secret`).

To create a realm, use the "Add realm" button in the drop-down that
opens when the mouse moves over the "master" in the top left corner.

To create a client, open the new realm and use the "Create" function in
the clients list inside the KeyCloak webinterface.

Initial settings

* Client ID - Must match the id set in the lua-resty config, e.g. `tuency_client`
* (opt) Name - the display name. Will be shown e.g. on the backlink if the
  SPA links to the account management page.
* Protocol - openid-connect
* Root URL - The url nginx can be reached by, e.g. `http://tuency_client`
* Redirect URIs - Valid Redirect URIs. One of these must match the
  redirect URLs used in the openresty configuration. You can use '`*`' as
  wild-card at the end, e.g. `http://tuencyone/*`.

Additional settings:

* Access Type - Can be set to public for development purposes
* Mappers - Specifies which attributes should be included in the tokens, see the section below

For one of the clients, e.g. `tuency_client`, add the following roles:

* `portaladmin`
* `tenantadmin`
* `orgaadmin`

The `portaladmin` can be manually assigned to admin users, `tenantadmin` and `orgaadmin` should only be assigned by the application as they need additional database entries.


#### Mappers

Mappers can be created via the client settings in the KeyCloak web interface.
The [docs/examples/server_block](./examples/server_block) uses three mappers: _username_, _clientroles_ and _realmroles_.

_full name_, _clientroles_ and _realmroles_
can be added using the _builtin_ button in the mappers tab.
Some mappers need further adjustments:

clientroles:
* Client ID: `tuency_client`.
  This must be the client for which the roles were defined
* Add to ID Token: true
* Token Claim Name: clientroles

realmroles:
* Add to ID Token: true
* Token Claim Name: realmroles

_username_ can be added using the _create_ button:
* Mapper Type: User Property
* Property: username
* Token Claim Name: username
* Claim JSON Type: String

### Activate Email-Verification and Passwort-Reset

In _Realm Settings_ -> _Email_
 * put in outgoing SMTP _Host_
 * put in  _From:_ Address for the sender
 * (optional) enter _Display From Address_
 * press test connection (to send an email to the address of the logged in user)
   and verify that the email came through.

In _Realm Settings_ -> _Login_:
 * activate _Verify Email_ (do not enable this before your KeyCloak can send emails and your admin account has a verifiable Email address!)
 * activate _Forgot password_


### Add initial set of users

To add the initial set of users, the "Add user" function in the "Users" list inside the KeyCloak web interface can be used.
After providing at least the new username, a "Credentials" tab should appear after clicking save.
The tab can be used to set a password or set required actions
for the next user login.
If the password is marked as temporary, the new user has to set a new one after the first login.

Once the user is created, the password can be reset via the password
forgotten option.

Assign the client-role `portaladmin` that was created for one the
clients to this new user.


### Set Theme

See [../keycloak_theme/README.md](../keycloak_theme/README.md).
